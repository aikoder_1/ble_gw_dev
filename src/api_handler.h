#ifndef _API_HANDLER_H
#define _API_HANDLER_H

#include <zephyr/types.h>

void api_handler(uint8_t *data, uint16_t len);

void api_get_board_info(uint8_t *data, uint16_t len);
void api_reboot(uint8_t *data, uint16_t len);
void api_factory_reset(uint8_t *data, uint16_t len);
void api_dfu(uint8_t *data, uint16_t len);
void api_find_me(uint8_t *data, uint16_t len);
void api_uart_config(uint8_t *data, uint16_t len);
void api_keep_alive(uint8_t *data, uint16_t len);

void api_ble_scan(uint8_t *data, uint16_t len);
void api_ble_connect(uint8_t *data, uint16_t len);
void api_ble_get_connected_list(uint8_t *data, uint16_t len);
void api_ble_write(uint8_t *data, uint16_t len);
void api_ble_read(uint8_t *data, uint16_t len);
void api_ble_notify_set(uint8_t *data, uint16_t len);
void api_ble_get_scan_params(uint8_t *data, uint16_t len);
void api_ble_set_scan_params(uint8_t *data, uint16_t len);
void api_ble_get_conn_params(uint8_t *data, uint16_t len);
void api_ble_set_conn_params(uint8_t *data, uint16_t len);
void api_ble_get_filter_1(uint8_t *data, uint16_t len);
void api_ble_set_filter_1(uint8_t *data, uint16_t len);
void api_ble_get_filter_2(uint8_t *data, uint16_t len);
void api_ble_set_filter_2(uint8_t *data, uint16_t len);

void api_ble_set_adv(uint8_t *data, uint16_t len);
void api_ble_set_discon(uint8_t *data, uint16_t len);
void api_ble_get_adv_params(uint8_t *data, uint16_t len);
void api_ble_set_adv_params(uint8_t *data, uint16_t len);

#endif // _API_HANDLER_H

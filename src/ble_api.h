#ifndef _API_BLE_H
#define _API_BLE_H

#include <zephyr/types.h>

// struct api_ble_scan_params
// {
//     uint8_t type;
//     uint16_t interval;
//     uint16_t window;
//     uint16_t timeout;
// };

// struct api_ble_conn_params
// {
//     uint16_t interval_min;
//     uint16_t interval_max;
//     uint16_t latency;
//     uint16_t timeout;
// };

// int api_ble_start_scan(struct api_ble_scan_params *scan_params);
// int api_ble_stop_scan(void);
// int api_ble_connect(const char *addr, struct api_ble_scan_params *scan_params, struct api_ble_conn_params *conn_params);
// int api_ble_disconnect(const char *addr);
// int api_ble_write(const char *addr, const char *data, int len);
// int api_ble_read(const char *addr, char *data, int len);
// int api_ble_notify_set();
// int api_ble_get_board_info();

#endif // _API_BLE_H

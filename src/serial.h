#ifndef Z_INC_SERIAL_H
#define Z_INC_SERIAL_H

#include <zephyr/types.h>

#define UART_RX_BUF_SIZE CONFIG_SERIAL_RX_BUFFER_SIZE // 串口接收缓存区大小
#define UART_TX_BUF_SIZE CONFIG_SERIAL_TX_BUFFER_SIZE // 串口发送缓存区大小

struct uart_rx_buf_t
{
    void *fifo_reserved;
    uint8_t data[UART_RX_BUF_SIZE];
    uint16_t len;
};

typedef int (*rx_proccess_t)(uint8_t *data, uint16_t len);

struct serial_inst
{
    int (*tx)(const uint8_t *const data, uint16_t len);
};

struct serial_inst *serial_init(rx_proccess_t rx_proccess_func);

#endif // Z_INC_SERIAL_H

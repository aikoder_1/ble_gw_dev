

#include <stdio.h>
#include <string.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h>

#include "ble.h"

int main(void)
{
    gw_ble_init();

    while (1)
    {
        k_sleep(K_SECONDS(1));
    }

    return 0;
}

#ifndef _BLE_BOARD_H
#define _BLE_BOARD_H

#include <zephyr/types.h>

enum
{
    BOARD_TYPE_BLE = 1,
    BOARD_TYPE_LORA = 2,
    BOARD_TYPE_ZIGBEE = 3,
};

enum
{
    BOARD_HW_NRF52840 = 1,
};

#pragma pack(1)
struct board_info
{
    uint8_t type;
    uint8_t fw_ver[3];
    uint8_t hw_ver[1];
    uint8_t mac[6];
};
#pragma pack()

#endif // _BLE_BOARD_H

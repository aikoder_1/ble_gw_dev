#ifndef BLE_GATT_DISCOVERY_H
#define BLE_GATT_DISCOVERY_H

#include <stdint.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/gatt.h>

// 描述符结构体
struct GATTDescriptor {
    uint16_t handle;
    struct bt_uuid *uuid;
};

// 特征结构体
struct GATTCharacteristic {
    uint16_t handle;
    uint16_t value_handle;
    struct bt_uuid *uuid;
    uint8_t properties;
    struct GATTDescriptor *descriptors;  // 动态分配
    int num_descriptors;
};

// 服务结构体
struct GATTService {
    uint16_t start_handle;
    uint16_t end_handle;
    struct bt_uuid *uuid;
    struct GATTCharacteristic *characteristics;  // 动态分配
    int num_characteristics;
};

// GATT 设备结构体
struct GATTDevice {
    struct GATTService *services;  // 动态分配
    int num_services;
};

// GATT 发现结果回调类型
typedef void (*gatt_discovery_complete_cb)(struct GATTDevice *device, int status);

// GATT 发现状态
enum discovery_status_t {
    DISCOVERY_SUCCESS,
    DISCOVERY_FAILED
};

// GATT 服务发现 API，带回调
void gatt_discover(struct bt_conn *conn, gatt_discovery_complete_cb callback);
void print_device_gatt_structure(struct GATTDevice *device);

#endif // BLE_GATT_DISCOVERY_H
